//Link = https://www.codingninjas.com/studio/problems/longest-common-substring_1235207?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos&leftPanelTab=0

int lcsTB(string &s, string &t){
    int n=s.size();
    int m=t.size();

    vector<vector<int>> dp(n+1, vector<int>(m+1, 0));
    int ans=0;

    for(int ind1=1;ind1<=n;ind1++){
        for(int ind2=1;ind2<=m;ind2++){
            if(s[ind1-1]==t[ind2-1]){
                dp[ind1][ind2] = 1+dp[ind1-1][ind2-1];
            }
            dp[ind1][ind2] = 0;
            
            ans = max(ans, dp[ind1][ind2]);
        }
    }
    return ans;
}

int lcsSP(string &s, string &t){
    int n = s.size();
    int m = t.size();

    vector<int> prev(m+1, 0), cur(m+1, 0);
    int ans = 0;

    for(int ind1=1;ind1<=n;ind1++){
        for(int ind2=1;ind2<=m;ind2++){
            if(s[ind1-1]==t[ind2-1]){
                cur[ind2] = 1+prev[ind2-1];
            }
            else cur[ind2]=0;  

            ans = max(ans, cur[ind2]);  
        }
        prev = cur;
    }
    return ans;
}

int lcs(string &str1, string &str2){
    // Write your code here.
    return lcsSP(str1, str2);
}
