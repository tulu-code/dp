#include <bits/stdc++.h>
using namespace std;

//using dp recursion
int fibo(int n, vector<int> &dp){
    if(n<=1)
        return n;
    if(dp[n]!=-1)
        return dp[n];
    dp[n] = fibo(n-1, dp)+fibo(n-2, dp);
    return dp[n];
}
// using tabulation
int fibo(int n){
    vector dp(n+1, -1);
    dp[0]=0;
    dp[1]=1;
    for(int i=2;i<=n;i++){
        dp[i]=dp[i-1]+dp[i-2];
    }
    return dp[n];
}

// space optimization
int fiboSP(int n){
    int prev1 = 1;
    int prev2 = 0;
    int curr_value=0;
    for(int i=2;i<=n;i++){
        curr_value = prev1 + prev2;
        prev2 = prev1;
        prev1 = curr_value;
    }
    return curr_value;
}

int main() {
	// your code goes here
	int n=5;
	vector<int> dp(n+1, -1);
	cout<<fiboSP(n)<<endl;
	return 0;
}

