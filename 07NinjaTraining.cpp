//https://www.codingninjas.com/studio/problems/ninja-s-training_3621003?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos&leftPanelTab=0

int train(int day, vector<vector<int>> &points, int last, vector<vector<int>> &dp){
    if(day==0){
        int max_point = 0;
        for(int task = 0;task<3;task++)
            if(task !=last)
                max_point = max(max_point, points[day][task]);
        return dp[day][last] = max_point;
    }

    if(dp[day][last]!=-1)
        return dp[day][last];

    int max_point=0;
    for(int task=0;task<3;task++){
        if(task != last){
            int point = points[day][task] + train(day-1, points, task, dp);
            max_point = max(max_point, point);
        }
    }
    return dp[day][last] = max_point;
}

int trainTB(int n, vector<vector<int>> &points){
    vector<vector<int>> dp(n, vector<int>(4, 0));

    for(int next=0;next<4;next++){
        for(int task=0;task<3;task++){
            if(task!=next){
                dp[0][next] = max(dp[0][next], points[0][task]);
            }
        }
    }

    for(int day=1;day<n;day++){
        for(int next=0;next<4;next++){
            for(int task=0;task<3;task++){
                if(task!=next){
                    dp[day][next] = max(dp[day][next], points[day][task]+dp[day-1][task]);
                }
            }
        }
    }

    return dp[n-1][3];
}

int trainSP(int n, vector<vector<int>> &points){
    vector<int> dp(4, 0);

    for(int next=0;next<4;next++){
        for(int task=0;task<3;task++){
            if(task!=next){
                dp[next] = max(dp[next], points[0][task]);
            }
        }
    }

    for(int day=1;day<n;day++){
        vector<int> temp(4, 0);
        for(int next=0;next<4;next++){
            for(int task=0;task<3;task++){
                if(task!=next){
                    temp[next] = max(temp[next], points[day][task]+dp[task]);
                }
            }
        }
        dp = temp;
    }

    return dp[3];
}


int ninjaTraining(int n, vector<vector<int>> &points)
{
    // Write your code here.
    //0th index hold last task and 1st index total points
    vector<vector<int>> dp(n, vector<int>(4, -1));
    return train(n-1, points, 3, dp);
}
