//Link = https://www.codingninjas.com/studio/problems/can-you-make_4244510?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos&leftPanelTab=0


int lcsSP(string &s, string &t){
    int n = s.size();
    int m = t.size();

    vector<int> prev(m+1, 0), cur(m+1, 0);

    for(int ind1=1;ind1<=n;ind1++){
        for(int ind2=1;ind2<=m;ind2++){
            if(s[ind1-1]==t[ind2-1]) cur[ind2] = 1+prev[ind2-1];
            else cur[ind2] = max(cur[ind2-1], prev[ind2]);
        }
        prev = cur;
    }
    return prev[m];
}

int canYouMake(string &s1, string &s2){
    // Write your code here.
    int subsize = lcsSP(s1, s2);
    int deletion = s1.size() -subsize;
    int insertion = s2.size()- subsize;

    return deletion+insertion;
}
