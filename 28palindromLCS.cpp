//Link = https://www.codingninjas.com/studio/problems/longest-palindromic-subsequence_842787?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos&count=25&page=1&search=longest%20pali&sort_entity=order&sort_order=ASC&leftPanelTab=0

#include <bits/stdc++.h> 

int lcsSP(string &s, string &t){
    int n = s.size();
    int m = t.size();
    
    vector<int> prev(m+1, 0), cur(m+1, 0);

    for(int ind1=1;ind1<=n;ind1++){
        for(int ind2=1;ind2<=m;ind2++){
            if(s[ind1-1]==t[ind2-1])
                cur[ind2] = 1+prev[ind2-1];
            else
                cur[ind2] = max(cur[ind2-1], prev[ind2]);
        }
        prev = cur;
    }
    return prev[m];
}

int longestPalindromeSubsequence(string s)
{
    // Write your code here.
    string t = s;
    reverse(t.begin(), t.end());

    return lcsSP(s, t);
}
