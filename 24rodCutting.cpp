//Link = https://www.codingninjas.com/studio/problems/rod-cutting-problem_800284?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos

int cut(int ind, int target, vector<int> &price, vector<int> &length){
	if(ind==0){
		int num = target/length[ind];
		return num*price[ind];
	}

	int notTake = 0+cut(ind-1, target, price, length);
	int take = 0;
	if(target>=length[ind])
		take = price[ind] + cut(ind, target-length[ind], price, length);

	return max(notTake, take);
}

int cutDP(int ind, int target, vector<int> &price, vector<int> &length, vector<vector<int>> &dp){
	if(ind==0){
		int num = target/length[ind];
		return dp[ind][target] = num*price[ind];
	}

	if(dp[ind][target]!=-1)
		return dp[ind][target];

	int notTake = 0+cutDP(ind-1, target, price, length, dp);
	int take = 0;
	if(target>=length[ind])
		take = price[ind] + cutDP(ind, target-length[ind], price, length, dp);

	return dp[ind][target] = max(notTake, take);
}

int cutTB(int n, int k, vector<int> &price, vector<int> &length){
	vector<vector<int>> dp(n, vector<int>(k+1, 0));

	for(int j=0;j<=k;j++){
		int num = j/length[0];
		dp[0][j] = num*price[0];
	}

	for(int ind=1;ind<n;ind++){
		for(int target=0;target<=k;target++){
			int notTake = 0+dp[ind-1][target];
			int take = 0;
			if(target>=length[ind])
				take = price[ind] + dp[ind][target-length[ind]];

			dp[ind][target] = max(notTake, take);
		}
	}

	return dp[n-1][k];
}

int cutSP(int n, int k, vector<int> &price, vector<int> &length){
	vector<int> dp(k+1, 0);

	for(int j=0;j<=k;j++){
		int num = j/length[0];
		dp[j] = num*price[0];
	}

	for(int ind=1;ind<n;ind++){
		for(int target=0;target<=k;target++){
			int notTake = 0+dp[target];
			int take = 0;
			if(target>=length[ind])
				take = price[ind] + dp[target-length[ind]];

			dp[target] = max(notTake, take);
		}
	}

	return dp[k];	
}

int cutRod(vector<int> &price, int n)
{
	// Write your code here.
	vector<int> length(n, 0);
	for(int i=0;i<n;i++)
		length[i]=i+1;

	//vector<vector<int>> dp(n, vector<int>(n+1, -1));

	return cutSP(n, n, price, length);
}

