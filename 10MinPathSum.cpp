//link https://www.codingninjas.com/studio/problems/minimum-path-sum_985349?interviewProblemRedirection=true
#include <bits/stdc++.h>

int sumPath(int row, int col, vector<vector<int>> &grid){
    if(row==0 && col==0)
        return grid[row][col];
    if(row<0 || col<0)
        return 1e9;
    int up = grid[row][col] + sumPath(row-1, col, grid);
    int left = grid[row][col] + sumPath(row, col-1, grid);

    return min(up, left);
}

int sumPathDP(int row, int col, vector<vector<int>> &grid, vector<vector<int>> &dp){
    if(row==0 && col==0)
        return dp[row][col] = grid[row][col];
    if(dp[row][col]!=-1)
        return dp[row][col];

    int up = INT_MAX, left = INT_MAX;
    if(row>0) up = grid[row][col] + sumPath(row-1, col, grid);
    if(col>0) left = grid[row][col] + sumPath(row, col-1, grid);

    return dp[row][col] = min(up, left);
}

int sumPathTB(int row, int col, vector<vector<int>> &grid){
    vector<vector<int>> dp(row, vector<int>(col, -1));
    for(int i=0;i<row;i++){
        for(int j=0;j<col;j++){
            if(i==0 && j==0)
                dp[i][j] = grid[i][j];
            else{
                int up=INT_MAX, left=INT_MAX;
                if(i>0) up = grid[i][j] + dp[i-1][j];
                if(j>0) left = grid[i][j] + dp[i][j-1];
                dp[i][j] = min(up, left);
            }
        }
    }
    return dp[row-1][col-1];
}

int sumPathSP(int row, int col, vector<vector<int>> &grid){
    vector<int> prev(col, -1);
    for(int i=0;i<row;i++){
        vector<int> cur(col, -1);
        for(int j=0;j<col;j++){
            if(i==0 && j==0)
                cur[j] = grid[i][j];
            else{
                int up = INT_MAX, left = INT_MAX;
                if(i>0) up = grid[i][j] + prev[j];
                if(j>0) left = grid[i][j] + cur[j-1];
                cur[j] = min(up, left);
            }
        }
        prev = cur;
    }
    return prev[col-1];
}

int minSumPath(vector<vector<int>> &grid) {
    // Write your code here.
    int row = grid.size();
    int col = grid[0].size();
    vector<vector<int>> dp(row, vector<int>(col, -1));
    return sumPath(row-1, col-1, grid);
}
