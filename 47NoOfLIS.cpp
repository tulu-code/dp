//Link = https://www.codingninjas.com/studio/problems/number-of-longest-increasing-subsequence_3751627?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos&leftPanelTabValue=PROBLEM

int findNumberOfLIS(vector<int> &arr)
{
    // Write your code here.
    int n = arr.size(), maxi=1;

    vector<int> dp(n, 1), count(n, 1);

    for(int ind=1;ind<n;ind++){
        for(int prev=0;prev<ind;prev++){
            if(arr[ind]>arr[prev] && dp[ind]<dp[prev]+1){
                dp[ind] = dp[prev]+1;
                count[ind] = count[prev];
            }
            else if(arr[ind]>arr[prev] && dp[ind]==dp[prev]+1)
                count[ind] += count[prev];
        }
        maxi = max(maxi, dp[ind]);
    }

    int res = 0;
    for(int i=0;i<n;i++){
        if(dp[i]==maxi)
            res += count[i];
    }

    return res;
}
