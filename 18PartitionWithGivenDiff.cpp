//Link = https://www.codingninjas.com/studio/problems/partitions-with-given-difference_3751628?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos&leftPanelTab=0

#include <bits/stdc++.h> 

int mod = 1e9+7;

int partSP(int n, int k, vector<int> &arr){
    vector<int> prev(k+1, 0);
    for(int j=0;j<=k;j++){
		if(j==0 && arr[0]==0)
			prev[j]=2;
		else if(j==0 || arr[0]==j)
			prev[j]=1;
		else prev[j]=0;
	}

    for(int ind=1;ind<n;ind++){
        vector<int> cur(k+1, 0);
        for(int target=0;target<=k;target++){
            int notTake = prev[target];
            int take = 0;
            if(target>=arr[ind])
                take = prev[target-arr[ind]];
            cur[target] = (take+notTake)%mod;
        }
        prev = cur;
    }
    return prev[k];
}
int countPartitions(int n, int d, vector<int> &arr) {
    // Write your code here.
    int totalSum = 0;
    for(int i=0;i<n;i++) totalSum += arr[i];

    if(totalSum-d<0 || (totalSum-d)%2) return 0;

    int target = (totalSum-d)/2;
    return partSP(n, target, arr);
}



