//Link = https://www.codingninjas.com/studio/problems/ninja-and-his-friends_3125885?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos&leftPanelTab=1

#include <bits/stdc++.h>

int maxChoc(int i, int j1, int j2, vector<vector<int>> &grid, int r, int c){    
    if(j1<0 || j1>=c || j2<0 || j2>=c)
        return 0;

    if(i==(r-1)){
        if(j1==j2) return grid[i][j1];
        return grid[i][j1] + grid[i][j2];
    }

    int maxi = 0;
    for(int dj1=-1;dj1<=1;dj1++){
        for(int dj2=-1;dj2<=1;dj2++){
            if(j1==j2) maxi = max(maxi, grid[i][j1] + maxChoc(i+1, j1+dj1, j2+dj2, grid, r, c));
            else
                maxi = max(maxi, grid[i][j1]+grid[i][j2] + maxChoc(i+1, j1+dj1, j2+dj2, grid, r, c));
        }
    }
    return maxi;
}

int maxChocDP(int i, int j1, int j2, vector<vector<int>> &grid, int r, int c, vector<vector<vector<int>>> &dp){
    if(j1<0 || j1>=c || j2<0 || j2>=c)
        return 0;
    
    if(i==(r-1)){
        if(j1==j2) return dp[i][j1][j2] = grid[i][j1];
        else return dp[i][j1][j2] = grid[i][j1] + grid[1][j2];
    }

    if(dp[i][j1][j2]!=-1)
        return dp[i][j1][j2];

    int maxi = 0;
    for(int dj1=-1;dj1<=1;dj1++){
        for(int dj2=-1;dj2<=1;dj2++){
            if(j1==j2) maxi = max(maxi, grid[i][j1] + maxChoc(i+1, j1+dj1, j2+dj2, grid, r, c));
            else
                maxi = max(maxi, grid[i][j1]+grid[i][j2] + maxChoc(i+1, j1+dj1, j2+dj2, grid, r, c));
        }
    }
    return maxi;
}

int maxChocTB(int r, int c, vector<vector<int>> &grid){
    vector<vector<vector<int>>>dp(r, vector<vector<int>>(c, vector<int>(c, -1)));

    for(int i=r-1;i>=0;i--){
        for(int j1=0;j1<c;j1++){
            for(int j2=0;j2<c;j2++){
                if(i==r-1){
                    if(j1==j2) dp[i][j1][j2] = grid[i][j1];
                    else dp[i][j1][j2] = grid[i][j1] + grid[i][j2];
                }
                else{
                    int maxi = 0;
                    for(int dj1=-1;dj1<=1;dj1++){
                        for(int dj2=-1;dj2<=1;dj2++){
                            if(j1+dj1>=0 && j1+dj1<c && j2+dj2>=0 && j2+dj2<c){
                                if(j1==j2) maxi = max(maxi, grid[i][j1] + dp[i+1][j1+dj1][j2+dj2]);
                                else
                                    maxi = max(maxi, grid[i][j1]+grid[i][j2] + dp[i+1][j1+dj1][j2+dj2]);
                            }
                        }
                    }
                    dp[i][j1][j2] = maxi;
                }
            }
        }
    }

    return dp[0][0][c-1];
}

int maxChocSP(int r, int c, vector<vector<int>> &grid){
    vector<vector<int>> prev(c, vector<int>(c, 0));

    for(int i=r-1;i>=0;i--){
        vector<vector<int>> cur(c, vector<int>(c, 0));
        for(int j1=0;j1<c;j1++){
            for(int j2=0;j2<c;j2++){
                if(i==r-1){
                    if(j1==j2) cur[j1][j2] = grid[i][j1];
                    else cur[j1][j2] = grid[i][j1] + grid[i][j2];
                }
                else{
                    int maxi = 0;
                    for(int dj1=-1;dj1<=1;dj1++){
                        for(int dj2=-1;dj2<=1;dj2++){
                            if(j1+dj1>=0 && j1+dj1<c && j2+dj2>=0 && j2+dj2<c){
                                if(j1==j2) maxi = max(maxi, grid[i][j1] + prev[j1+dj1][j2+dj2]);
                                else
                                    maxi = max(maxi, grid[i][j1]+grid[i][j2] + prev[j1+dj1][j2+dj2]);
                            }
                        }
                    }
                    cur[j1][j2] = maxi;
                }
            }
        }
        prev = cur;
    }
    return prev[0][c-1];
}

int maximumChocolates(int r, int c, vector<vector<int>> &grid) {
    // Write your code here
    //vector<vector<vector<int>>>dp(r, vector<vector<int>>(c, vector<int>(c, -1)));
    return maxChocSP(r, c, grid);
}
