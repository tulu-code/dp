//Link = https://www.codingninjas.com/studio/problems/partition-equal-subset-sum_892980?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos&leftPanelTab=0

bool partition(int ind, int target, vector<int> &arr){
	if(target==0)
		return true;
	if(ind==0)
		return arr[ind]==target;

	bool notTake = partition(ind-1, target, arr);
	bool take = false;
	if(target>=arr[ind])
		take = partition(ind-1, target-arr[ind], arr);

	return notTake|take;
}

bool partitionDP(int ind, int target, vector<int> &arr, vector<vector<int>> &dp){
	if(target==0){
		dp[ind][target]=1;
		return true;
	}
	if(ind==0){
		if(target==arr[ind]){
			dp[ind][target] = 1;
			return true;
		}
		else{
			dp[ind][target] = 0;
			return false;
		}
	}

	if(dp[ind][target]!=-1)
		return dp[ind][target]==1;
	
	bool notTake = partitionDP(ind-1, target, arr, dp);
	bool take = false;
	if(target>=arr[ind])
		take = partitionDP(ind-1, target-arr[ind], arr, dp);
	
	return notTake|take;
}

bool partitionTB(int n, int k, vector<int> &arr){
	vector<vector<bool>> dp(n, vector<bool>(k+1, false));

	for(int i=0;i<n;i++)
		dp[i][0] = true;
	if(arr[0]<=k) dp[0][arr[0]] = true;

	for(int ind=1;ind<n;ind++){
		for(int target=1;target<=k;target++){
			bool notTake = dp[ind-1][target];
			bool take = false;
			if(target>=arr[ind])
				take = dp[ind-1][target-arr[ind]];
			
			dp[ind][target] = notTake|take;
		}
	}

	return dp[n-1][k];
}

bool partitionSP(int n, int k, vector<int> &arr){
	vector<bool> prev(k+1, false);

	prev[0]=true;
	if(arr[0]<=k) prev[arr[0]]=true;

	for(int ind=1;ind<n;ind++){
		vector<bool> cur(k+1, false);
		cur[0] = true;
		for(int target=1;target<=k;target++){
			bool notTake = prev[target];
			bool take = false;
			if(target>=arr[ind])
				take = prev[target-arr[ind]];
			cur[target] = notTake|take;
		}
		prev = cur;
	}

	return prev[k];
}

bool canPartition(vector<int> &arr, int n)
{
	// Write your code here.
	int totalSum = 0;
	for(int i=0;i<n;i++)
		totalSum += arr[i];
	if(totalSum%2)
		return false;
	
	int target = totalSum/2;
	//vector<vector<int>> dp(n, vector<int>(target+1, -1));
	return partitionSP(n-1, target, arr);
}

