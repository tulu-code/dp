//Link = https://www.codingninjas.com/studio/problems/divisible-set_3754960?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos&leftPanelTabValue=PROBLEM


vector<int> divisable(vector<int> arr){
    int n = arr.size();
    vector<int> dp(n, 1), hash(n);
    int lastIndex = 0, maxlis=0;

    sort(arr.begin(), arr.end());

    for(int ind=1;ind<n;ind++){
        hash[ind]=ind;
        for(int prev=0;prev<ind;prev++){
            if(arr[ind]%arr[prev]==0 && dp[ind]<dp[prev]+1){
                dp[ind] = dp[prev]+1;
                hash[ind] = prev;
            }
        }

        if(dp[ind]>maxlis){
            maxlis = dp[ind];
            lastIndex = ind;
        }
    }

    vector<int> result;
    result.push_back(arr[lastIndex]);

    while(lastIndex!=hash[lastIndex]){
        lastIndex = hash[lastIndex];
        result.push_back(arr[lastIndex]);
    }

    return result;
}

vector<int> divisibleSet(vector<int> &arr)
{
    // Write your code here.
    return divisable(arr);
}
