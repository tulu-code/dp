//Link = https://www.codingninjas.com/studio/problems/0-1-knapsack_920542?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos&leftPanelTab=0

#include <bits/stdc++.h> 

int knaprec(int ind, int wt, vector<int> weight, vector<int> value){
	if(ind==0){
		if(wt<weight[0]) return 0;
		else return value[0];
	}
	int notTake = 0+knaprec(ind-1, wt, weight, value);
	int take = INT_MIN;
	if(wt>=weight[ind])
		take = value[ind] + knaprec(ind-1, wt-weight[ind], weight, value);
	return max(notTake, take);
}

int knapDP(int ind, int wt, vector<int> weight, vector<int> value, vector<vector<int>> &dp){
	if(ind==0){
		if(wt<weight[0]) return dp[ind][wt]=0;
		else return dp[ind][wt]=value[ind];
	}
	if(dp[ind][wt]!=-1)
		return dp[ind][wt];
	int notTake = 0+knapDP(ind-1, wt, weight, value, dp);
	int take = INT_MIN;
	if(wt>=weight[ind])
		take = value[ind] + knapDP(ind-1, wt-weight[ind], weight, value, dp);
	return dp[ind][wt] = max(notTake, take);
}

int knapTB(int n, int wt, vector<int> weight, vector<int> value){
	vector<vector<int>> dp(n, vector<int>(wt+1, 0));

	for(int j=weight[0];j<=wt;j++)
		dp[0][j] = value[0];

	for(int ind=1;ind<n;ind++){
		for(int target=0;target<=wt;target++){
			int notTake = 0+dp[ind-1][target];
			int take = INT_MIN;
			if(target>=weight[ind])
				take = value[ind] + dp[ind-1][target-weight[ind]];

			dp[ind][target] = max(notTake, take);
		}
	}
	return dp[n-1][wt];
}

int knapSP(int n, int wt, vector<int> weight, vector<int> value){
	vector<int> prev(wt+1, 0);

	for(int j=weight[0];j<=wt;j++)
		prev[j] = value[0];
	
	for(int ind=1;ind<n;ind++){
		vector<int> cur(wt+1, 0);
		for(int target=0;target<=wt;target++){
			int notTake = 0+prev[target];
			int take = INT_MIN;
			if(target>=weight[ind])
				take = value[ind] + prev[target-weight[ind]];

			cur[target] = max(notTake, take);
		}
		prev = cur;
	}
	return prev[wt];
}

// more space optimization
int knapmSP(int n, int wt, vector<int> weight, vector<int> value){
	vector<int> dp(wt+1, 0);

	for(int j=weight[0];j<=wt;j++)
		dp[j] = value[0];

	for(int ind=1;ind<n;ind++){
		for(int target=wt;target>=0;target--){
			int notTake = 0+dp[target];
			int take = INT_MIN;
			if(target>=weight[ind])
				take = value[ind] + dp[target-weight[ind]];
			
			dp[target] = max(notTake, take);
		}
	}

	return dp[wt];
}

int knapsack(vector<int> weight, vector<int> value, int n, int maxWeight) 
{
	// Write your code here
	//vector<vector<int>> dp(n, vector<int>(maxWeight+1, -1));
	return knapmSP(n, maxWeight, weight, value);
}
