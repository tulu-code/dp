#include<bits/stdc++.h>
using namespace std;


string lcsTB(string s, string t){
    int n = s.size();
    int m = t.size();
    
    vector<vector<int>> dp(n+1, vector<int>(m+1, 0));
    
    for(int ind1=1;ind1<=n;ind1++){
        for(int ind2=1;ind2<=m;ind2++){
            if(s[ind1-1]==t[ind2-1]) dp[ind1][ind2] = 1+dp[ind1-1][ind2-1];
            else dp[ind1][ind2] = max(dp[ind1-1][ind2], dp[ind1][ind2-1]);
        }
    }
    
    int strSize = dp[n][m];
    cout<<strSize<<endl;
    
    string st;
    for(int i=0;i<strSize;i++) st += '$';
    int index = strSize-1;
    //cout<<st;
    
    int i=n, j=m;
    while(i>0 && j>0){
        if(s[i-1]==t[j-1]){
            st[index] = s[i-1];
            index--;
            i--;
            j--;
        }
        else{
            if(dp[i-1][j]>dp[i][j-1])
                i--;
            else j--;
        }
    }
    return st;
}

int main(){
    string s = "abcde";
    string t = "bcgek";
    
    string st = lcsTB(s, t);
    
    cout<<st;
    
}
