//link https://www.codingninjas.com/studio/problems/maze-obstacles_977241?interviewProblemRedirection=true&leftPanelTabValue=PROBLEM
int mod = (int)(1e9 + 7);

// int obstacle(int n, int m, vector<vector<int>> &mat){
//     if(n==0 && m==0){
//         if(mat[n][m]==0)
//             return 1;
//         else return 0;
//     }

//     if(n<0 || m<0)
//         return 0;
//     if(mat[n][m]==-1)
//         return 0;
//     return (obstacle(n-1, m, mat) + obstacle(n, m-1, mat))%mod;
// }

int traverse(int n, int m, vector<vector<int>> &mat){
    if(n<0 || m<0) return 0;
    if(mat[n][m]==-1) return 0;
    if(n==0 && m==0) return 1;
    return (traverse(n-1, m, mat) + traverse(n, m-1, mat))%mod;
}

int obstacleDP(int n, int m, vector<vector<int>> &mat, vector<vector<int>> &dp){
    if(n==0 && m==0){
        if(mat[n][m]==0)
            return dp[n][m] = 1;
        else return dp[n][m] = 0;
    }

    if(n<0 || m<0)
        return 0;
    if(mat[n][m]==-1)
        return dp[n][m] = 0;

    if(dp[n][m]!=-1) return dp[n][m];

    return (obstacle(n-1, m, mat) + obstacle(n, m-1, mat))%mod;    
}

int obstacleTB(int n, int m, vector<vector<int>> &mat){
    vector<vector<int>> dp(n, vector<int>(m, -1));

    for(int row = 0;row<n;row++){
        for(int col=0;col<m;col++){
            if(row==0 && col==0){
                if(mat[row][col]==0)
                    dp[row][col] = 1;
                else dp[row][col] = 0;
            }
            else if(mat[row][col]==-1) dp[row][col] = 0;
            else{
                int up=0, left = 0;
                if(row>0) up = dp[row-1][col];
                if(col>0) left = dp[row][col-1];
                dp[row][col] = (up+left)%mod;
            }
        }
    }
    return dp[n-1][m-1];
}

//best runtime
// int obstacleSP(int n, int m, vector<vector<int>> &mat){
//     vector<int> prev(m, 0);
//     for(int row=0;row<n;row++){
//         vector<int> cur(m, 0);
//         for(int col=0;col<m;col++){
//             if(row==0 && col==0){
//                 if(mat[row][col]==0)
//                     cur[col] = 1;
//                 else cur[col] = 0;
//             }
//             else if(mat[row][col]==-1) cur[col] = 0;
//             else{
//                 int up = 0, left = 0;
//                 if(row>0) up = prev[col];
//                 if(col>0) left = cur[col-1];
//                 cur[col] = (up+left)%mod;
//             }
//         }
//         prev = cur;
//     }
//     return prev[m-1];
// }


int traverseSP(int n, int m, vector<vector<int>> &mat){
    vector<int> prev(m, 0);
    for(int row=0;row<n;row++){
        vector<int> curr(m, 0);
        for(int col=0;col<m;col++){
            if(mat[row][col] == -1) curr[col]=0;
            else if(row==0 && col==0) curr[col]=1;
            else{
                int up = prev[col];
                int left = 0;
                if(col>0)
                    left = curr[col-1];

                curr[col] = (up+left)%mod;
            }
        }
        prev = curr;
    }
    return prev[m-1];
}

int mazeObstacles(int n, int m, vector< vector< int> > &mat) {
    // Write your code here
    //vector<vector<int>> dp(n, vector<int>(m, -1));
    return obstacleSP(n, m, mat);
}
