//Link = https://www.codingninjas.com/studio/problems/maximum-path-sum-in-the-matrix_797998?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos&leftPanelTab=0

#include <bits/stdc++.h> 

int getMax(int row, int col, vector<vector<int>> &matrix, int n, int m){
    if(row==0)
        return matrix[row][col];

    int up = INT_MIN, dg_left = INT_MIN, dg_right = INT_MIN;

    up = matrix[row][col] + getMax(row-1, col, matrix, n, m);
    if(col>0) dg_left = matrix[row][col] + getMax(row-1, col-1, matrix, n, m);
    if(col<m-1) dg_right = matrix[row][col] + getMax(row-1, col+1, matrix, n, m);

    return max(max(up, dg_left), dg_right);
}

int getMaxDP(int row, int col, vector<vector<int>> &matrix, int n, int m, vector<vector<int>> &dp){
    if(row==0)
        return dp[row][col] = matrix[row][col];

    if(dp[row][col] !=INT_MIN)
        return dp[row][col];

    int up = INT_MIN, dg_left = INT_MIN, dg_right = INT_MIN;
    up = matrix[row][col] + getMax(row-1, col, matrix, n, m);
    if(col>0) dg_left = matrix[row][col] + getMax(row-1, col-1, matrix, n, m);
    if(col<m-1) dg_right = matrix[row][col] + getMax(row-1, col+1, matrix, n, m);

    return dp[row][col] = max(max(up, dg_left), dg_right);
}

int getMaxTB(vector<vector<int>> &matrix){
    int n = matrix.size();
    int m = matrix[0].size();

    vector<vector<int>> dp(n, vector<int>(m, INT_MIN));

    for(int i=0;i<n;i++){
        for(int j=0;j<m;j++){
            if(i==0){
                dp[i][j] = matrix[i][j];
            }
            else{
                int up = INT_MIN, dg_left = INT_MIN, dg_right = INT_MIN;
                up = matrix[i][j] + dp[i-1][j];
                if(j>0) dg_left = matrix[i][j] + dp[i-1][j-1];
                if(j<m-1) dg_right = matrix[i][j] + dp[i-1][j+1];
                dp[i][j] = max(max(up, dg_left), dg_right);
            }
        }
    }
    
    int maxSum = INT_MIN;
    for(int j=0;j<m;j++)
        maxSum = max(maxSum, dp[n-1][j]);

    return maxSum;
}

int getMaxSP(vector<vector<int>> &matrix){
    int n = matrix.size();
    int m = matrix[0].size();

    vector<int> prev(m, INT_MIN);

    for(int i=0;i<n;i++){
        vector<int> cur(m, INT_MIN);
        for(int j=0;j<m;j++){
            if(i==0){
                cur[j] = matrix[i][j];
            }
            else{
                int up = INT_MIN, dg_left = INT_MIN, dg_right = INT_MIN;
                up = matrix[i][j] + prev[j];
                if(j>0) dg_left = matrix[i][j] + prev[j-1];
                if(j<m-1) dg_right = matrix[i][j] + prev[j+1];
                cur[j] = max(max(up, dg_left), dg_right);
            }
        }
        prev = cur;
    }
    
    int maxSum = INT_MIN;
    for(int j=0;j<m;j++)
        maxSum = max(maxSum, prev[j]);

    return maxSum;
}

int getMaxPathSum(vector<vector<int>> &matrix)
{
    //  Write your code here.
    //int n = matrix.size();
    //int m = matrix[0].size();
    //int maxsum = INT_MIN;

    //vector<vector<int>> dp(n, vector<int>(m, INT_MIN));
    
    //for(int j=0;j<m;j++){
    //    dp[n-1][j] = max(dp[n-1][j], getMaxTB(matrix));
    //    maxsum = max(maxsum, getMaxDP(n-1, j, matrix, n, m, dp));
    //}
    return getMaxSP(matrix);
    //return maxsum;
}
