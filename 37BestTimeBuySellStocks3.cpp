https://www.codingninjas.com/studio/problems/buy-and-sell-stock_1071012?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos&leftPanelTab=0

int profit(int ind, int buy, int cap, vector<int> &prices, int n){
    if(cap==0) return 0;
    if(ind==n) return 0;

    int maxprofit = 0;
    if(buy)
        maxprofit = max(-prices[ind] + profit(ind+1, 0, cap, prices, n),
                    0 + profit(ind+1, 1, cap, prices, n));
    else
        maxprofit = max(prices[ind] + profit(ind+1, 1, cap-1, prices, n),
                    0 + profit(ind+1, 0, cap, prices, n));

    return maxprofit;
}

int profitDP(int ind, int buy, int cap, vector<int> &prices, int n, vector<vector<vector<int>>> &dp){
    if(cap==0) return 0;
    if(ind==n) return 0;

    if(dp[ind][buy][cap]!=-1)
        return dp[ind][buy][cap];

    int maxprofit = 0;
    if(buy)
        maxprofit = max(-prices[ind] + profitDP(ind+1, 0, cap, prices, n, dp),
                    0 + profitDP(ind+1, 1, cap, prices, n, dp));
    else
        maxprofit = max(prices[ind] + profitDP(ind+1, 1, cap-1, prices, n, dp),
                    0 + profitDP(ind+1, 0, cap, prices, n, dp));

    return dp[ind][buy][cap] = maxprofit;
}

int profitTB(vector<int> &prices){
    int n = prices.size();
    vector<vector<vector<int>>> dp(n+1, vector<vector<int>>(2, vector<int>(3, 0)));
    //base cases
    for(int i=0;i<=n;i++){
        for(int j=0;j<2;j++){
            dp[i][j][0] = 0;
        }
    }

    for(int j=0;j<2;j++){
        for(int k=0;k<3;k++){
            dp[n][j][k]=0;
        }
    }

    for(int ind=n-1;ind>=0;ind--){
        for(int buy=0;buy<2;buy++){
            for(int cap=1;cap<3;cap++){
                int maxprofit=0;
                if(buy)
                    maxprofit = max(-prices[ind] + dp[ind+1][0][cap],
                                    0 + dp[ind+1][1][cap]);
                else
                    maxprofit = max(prices[ind] + dp[ind+1][1][cap-1],
                                    0 + dp[ind+1][0][cap]);
                
                dp[ind][buy][cap] = maxprofit;
            }
        }
    }
    return dp[0][1][2];
}

int profitSP(vector<int> &prices){
    int n = prices.size();
    vector<vector<int>> prev(2, vector<int>(3, 0)), cur(2, vector<int>(3, 0));
    
    for(int ind=n-1;ind>=0;ind--){
        for(int buy=0;buy<2;buy++){
            for(int cap=1;cap<3;cap++){
                int maxprofit=0;
                if(buy)
                    maxprofit = max(-prices[ind] + prev[0][cap],
                                    0 + prev[1][cap]);
                else
                    maxprofit = max(prices[ind] + prev[1][cap-1],
                                    0 + prev[0][cap]);
                
                cur[buy][cap] = maxprofit;
            }
        }
        prev = cur;
    }
    return prev[1][2];
}


int maxProfit(vector<int>& prices)
{
    // Write your code here.
    int n = prices.size();
    //vector<vector<vector<int>>> dp(n, vector<vector<int>>(2, vector<int>(3, -1)));
    return profitSP(prices);
}
