//Link = https://www.codingninjas.com/studio/problems/subsequence-counting_3755256?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos&leftPanelTab=0

int mod = 1e9+7;
int ds(int i, int j, string s, string t){
	if(j<0) return 1;
	if(i<0) return 0;

	if(s[i]==t[j])
		return (ds(i-1, j-1, s, t) + ds(i-1, j, s, t))%mod;
	else return ds(i-1, j, s, t);
}

int dsDP(int i, int j, string s, string t, vector<vector<int>> dp){
	if(j<0) return 1;
	if(i<0) return 0;

	if(dp[i][j]!=-1)
		return dp[i][j];

	if(s[i]==t[j])
		return dp[i][j] = (dsDP(i-1, j-1, s, t, dp) + dsDP(i-1, j, s, t, dp))%mod;
	else return dp[i][j] = dsDP(i-1, j, s, t, dp);
}

int dsTB(int n, int m, string s, string t){
	vector<vector<int>> dp(n+1, vector<int>(m+1, 0));
	for(int i=0;i<=n;i++) dp[i][0]=1;
	for(int j=1;j<=m;j++) dp[0][j]=0; // as dp[0][0] should be 1

	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			if(s[i-1]==t[j-1])
				dp[i][j] = (dp[i-1][j-1] + dp[i-1][j])%mod;
			else dp[i][j] = dp[i-1][j];
		}
	}

	return dp[n][m];
}

int dsSP(int n, int m, string s, string t){
	vector<int> dp(m+1, 0);
	dp[0]=1;

	for(int i=1;i<=n;i++){
		for(int j=m;j>=0;j--){
			if(s[i-1]==t[j-1])
				dp[j] = (dp[j-1] + dp[j])%mod;
		}
	}

	return dp[m];
}

int distinctSubsequences(string &str, string &sub)
{
	// Write your code here.
	int n = str.size();
	int m = sub.size();

	//vector<vector<int>> dp(n, vector<int>(m, -1));


	return dsSP(n, m, str, sub);
}

