//Link = https://www.codingninjas.com/studio/problems/stocks-are-profitable_893405?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos

#include <bits/stdc++.h> 
int maximumProfit(vector<int> &prices){
    // Write your code here.
    int mini = prices[0], maxprofit = 0;
    for(int i=1;i<prices.size();i++){
        int profit = prices[i]-mini;
        maxprofit = max(profit, maxprofit);
        mini = min(mini, prices[i]);
    }
    return maxprofit;
}
