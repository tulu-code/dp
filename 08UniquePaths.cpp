//link = https://www.codingninjas.com/studio/problems/total-unique-paths_1081470?interviewProblemRedirection=true
#include <bits/stdc++.h>

int path(int m, int n){
	if(m==0 && n==0){
		return 1;
	}

	if(m<0 || n<0)
		return 0;
	return path(m-1, n) + path(m, n-1);
}

int pathDP(int m, int n, vector<vector<int>> &dp){
	if(m==0 && n==0){
		return dp[0][0] = 1;
	}
	if(m<0 || n<0){
		return 0;
	}

	if(dp[m][n]!=-1)
		return dp[m][n];
	

	return dp[m][n] = pathDP(m-1, n, dp)+pathDP(m, n-1, dp);
}

int pathTB(int m, int n){
	vector<vector<int>> dp(m, vector<int>(n, -1));
	dp[0][0] = 1;

	for(int row=0;row<m;row++){
		for(int col=0;col<n;col++){
			int up = 0, left = 0;
			if(row-1>=0)
				up = dp[row-1][col];
			if(col-1>=0)
				left = dp[row][col-1];
			if(row!=0 || col !=0)
				dp[row][col] = up+left;
		}
	}

	return dp[m-1][n-1];
}

int pathSP(int m, int n){
	vector<int> prev(n, 0);
	for(int row=0;row<m;row++){
		vector<int> temp(n, 0);
		for(int col=0;col<n;col++){
			if(row==0 && col==0) temp[col]=1;
			else{
				int up = 0, left = 0;
				up = prev[col];
				if(col-1>=0) left = temp[col-1];
				temp[col] = up+left;
			}
		}
		prev = temp;
	}
	return prev[n-1];
}

int uniquePaths(int m, int n) {
	// Write your code here.
	vector<vector<int>> dp(m, vector<int>(n, -1));
	return pathSP(m, n);
}
