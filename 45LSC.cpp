//Link = https://www.codingninjas.com/studio/problems/longest-string-chain_3752111?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos&leftPanelTabValue=PROBLEM

bool compare(string s, string t){
    if(s.size()!=t.size()+1) return false;

    int ptr1 = 0, ptr2=0;

    while(ptr1<s.size()){
        if(ptr2<t.size() && s[ptr1]==t[ptr2]){
            ptr1++;ptr2++;
        }
        else{
            ptr1++;
        }
    }

    if(ptr1 == s.size() && ptr2 == t.size()) return true;
    return false;
}

bool cmp(string &s, string &t){
    return s.size()<t.size();
}

int lsc(vector<string> &arr){
    int n = arr.size();
    vector<int> dp(n, 1);
    int maxli=0;

    // sort the array according to the length
    sort(arr.begin(), arr.end(), cmp);

    for(int ind=1;ind<n;ind++){
        for(int prev=0;prev<ind;prev++){
            if(compare(arr[ind], arr[prev]) && dp[ind]<dp[prev]+1)
                dp[ind] = dp[prev]+1;
        }
        if(dp[ind]>maxli)
            maxli = dp[ind];
    }
    return maxli;
}

int longestStrChain(vector<string> &arr){
    // Write your code here.
    return lsc(arr);
}
