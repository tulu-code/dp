//Link = https://www.codingninjas.com/studio/problems/longest-common-subsequence_624879?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos&leftPanelTab=0

#include<bits/stdc++.h>
int lcsRec(int ind1, int ind2, string s, string t){
	if(ind1<0 || ind2<0)
		return 0;
	
	if(s[ind1]==t[ind2])
		return 1+lcsRec(ind1-1, ind2-1, s, t);

	return 0 + max(lcsRec(ind1-1, ind2, s, t), lcsRec(ind1, ind2-1, s, t));
}

int lcsDP(int ind1, int ind2, string s, string t, vector<vector<int>> dp){
	if(ind1<0 || ind2<0)
		return 0;

	if(dp[ind1][ind2] !=-1)
		return dp[ind1][ind2];
	
	if(s[ind1] == t[ind2])
		return dp[ind1][ind2] = 1+lcsDP(ind1-1, ind2-1, s, t, dp);

	return dp[ind1][ind2] = 0+max(lcsDP(ind1-1, ind2, s, t, dp), lcsDP(ind1, ind2-1, s, t, dp));
}

int lcsTB(int n, int m, string s, string t){
	vector<vector<int>> dp(n+1, vector<int>(m+1, 0));

	for(int ind1=1;ind1<=n;ind1++){
		for(int ind2=1;ind2<=m;ind2++){
			if(s[ind1-1]==t[ind2-1]) dp[ind1][ind2] = 1+dp[ind1-1][ind2-1];
			else dp[ind1][ind2] = max(dp[ind1-1][ind2], dp[ind1][ind2-1]);
		}
	}
	return dp[n][m];
}

int lcsSP(int n, int m, string s, string t){
	vector<int> prev(m+1, 0), cur(m+1, 0);

	for(int ind1 = 1;ind1<=n;ind1++){
		for(int ind2=1;ind2<=m;ind2++){
			if(s[ind1-1]== t[ind2-1]) cur[ind2] = 1+prev[ind2-1];
			else cur[ind2] = max(prev[ind2], cur[ind2-1]);
		}
		prev = cur;
	}
	return prev[m];
}

int lcs(string s, string t)
{
	//Write your code here
	int n = s.size();
	int m = t.size();

	//vector<vector<int>> dp(n, vector<int>(m, -1));

	return lcsSP(n, m, s, t);
}
