//Link = https://www.codingninjas.com/studio/problems/minimum-insertions-to-make-palindrome_985293?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos&leftPanelTab=0


int pallin(string &s){
	string t = s;
	reverse(t.begin(), t.end());

	int n = s.size();

	vector<int> prev(n+1, 0), cur(n+1, 0);
	int maxpallin = 0;

	for(int ind1=1;ind1<=n;ind1++){
		for(int ind2=1;ind2<=n;ind2++){
			if(s[ind1-1]==t[ind2-1]) cur[ind2] = 1+prev[ind2-1];
			else cur[ind2] = max(cur[ind2-1], prev[ind2]);
		}
		prev = cur;
	}
	return n-prev[n];
}

int minimumInsertions(string &str)
{
	// Write your code here.
	return pallin(str);
}

