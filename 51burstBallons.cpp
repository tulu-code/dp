//Link = https://www.codingninjas.com/studio/problems/mining-diamonds_4244494?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos&leftPanelTabValue=PROBLEM

#include <bits/stdc++.h>

int coin(int i, int j, vector<int> &a){
	if(i>j) return 0;

	int maxi = 0;
	for(int k=i;k<=j;k++){
		int coins = a[i-1]*a[k]*a[j+1] + coin(i, k-1, a) + coin(k+1, j, a);
		maxi = max(maxi, coins);
	}
	return maxi;
}

int coinDP(int i, int j, vector<int> &a, vector<vector<int>> &dp){
	if(i>j) return 0;

	if(dp[i][j]!=-1)
		return dp[i][j];

	int maxi = 0;
	for(int k=i;k<=j;k++){
		int coins = a[i-1]*a[k]*a[j+1] + coinDP(i, k-1, a, dp) + coinDP(k+1, j, a, dp);
		maxi = max(maxi, coins);
	}
	return dp[i][j] = maxi;
}

int coinTB(vector<int> &a){
	int n = a.size();

	a.push_back(1);
	a.insert(a.begin(), 1);

	vector<vector<int>> dp(n+2, vector<int>(n+2, 0));
	// if we use n+1 then if j=n and k=j then dp[k+1] means dp[n+1

	for(int i=n;i>=1;i--){
		for(int j=1;j<=n;j++){
			if(i>j) continue;

			int maxi = 0;
			for(int k=i;k<=j;k++){
				int coins = a[i-1]*a[k]*a[j+1] + dp[i][k-1] + dp[k+1][j];
				maxi = max(maxi, coins);
			}
			dp[i][j] = maxi;
		}
	}
	return dp[1][n];
}

int maxCoins(vector<int>& a)
{
	// Write your code here.
	//int n = a.size();

	//a.push_back(1);
	//a.insert(a.begin(), 1);

	//vector<vector<int>> dp(n+1, vector<int>(n+1, -1));

	return coinTB(a);
}
