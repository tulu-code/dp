#include<bits/stdc++.h>
using namespace std;

int jump(int n, vector<int> &heights, vector<int> &dp, int k){
    if(n==0)
        return 0;
    if(dp[n]!=-1)
        return dp[n];
    int Min = INT_MAX;
    for(int i=1;i<=k;i++){
        int prev_index = n-i;
        if(prev_index>=0){
            int cost = jump(prev_index, heights, dp, k) + abs(heights[n]-heights[prev_index]);
            Min = min(Min, cost);
        }
    }
    return dp[n]=Min;
}

int jumpSP(int n, vector<int>heights, int k){
    vector<int> cost(n+1, 0);
    for(int index=1;index<=n;index++){
        int Min = INT_MAX;
        for(int i=1;i<=k;i++){
            int prev_index=index-i;
            if(prev_index>=0){
                int curr_cost = cost[prev_index]+abs(heights[index]-heights[prev_index]);
                Min = min(Min, curr_cost);
            }
        }
        cost[index]=Min;
    }
    return cost[n];
}

int frogjump(int n, vector<int> &heights, int k){
    vector<int> dp(n, -1);
    dp[0]=0;
    return jumpSP(n-1, heights,k);
}

int main(){
    int n = 6;
    vector<int> heights = {30, 10, 60, 10, 60, 50};
    cout<<frogjump(n, heights, 3);
}
