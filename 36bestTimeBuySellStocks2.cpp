//Link = https://www.codingninjas.com/studio/problems/selling-stock_630282?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos&leftPanelTab=0

#include<bits/stdc++.h>

long maxProfit(int ind, int buy, long *values, int n){
    if(ind==n)
        return 0;
    long profit = 0;
    if(buy)
        profit = max(-values[ind]+maxProfit(ind+1, 0, values, n),
                    0+maxProfit(ind+1, 1, values, n));
    else
        profit = max(values[ind]+maxProfit(ind+1, 1, values, n),
                    0+maxProfit(ind+1, 0, values, n));
    
    return profit;
}

long maxProfitDP(int ind, int buy, long *values, int n, vector<vector<long>> &dp){
    if(ind==n)
        return 0;
    if(dp[ind][buy]!=-1)
        return dp[ind][buy];
    
    long profit = 0;
    if(buy)
        profit = max(-values[ind]+maxProfitDP(ind+1, 0, values, n, dp),
                    0+maxProfitDP(ind+1, 1, values, n, dp));
    else
        profit = max(values[ind]+maxProfitDP(ind+1, 1, values, n, dp),
                    0+maxProfitDP(ind+1, 0, values, n, dp));
    
    return dp[ind][buy] = profit;
}

long maxProfitTB(int n, long *values){
    vector<vector<long>> dp(n+1, vector<long>(2, 0));
    dp[n][0]=dp[n][1]=0;

    for(int ind=n-1;ind>=0;ind--){
        for(int buy = 0;buy<=1;buy++){
            long profit = 0;
            if(buy)
                profit = max(-values[ind]+dp[ind+1][0],
                    0+dp[ind+1][1]);
            else
                profit = max(values[ind]+dp[ind+1][1],
                            0+dp[ind+1][0]);

            dp[ind][buy] = profit;
        }
    }
    return dp[0][1];
}

long maxProfitSP(int n, long *values){
    vector<long> cur(2, 0), prev(2, 0);
    prev[0]=prev[1]=0;

    for(int ind=n-1;ind>=0;ind--){
        for(int buy=0;buy<=1;buy++){
            long profit = 0;
            if(buy)
                profit = max(-values[ind] + prev[0], 0+prev[1]);
            else
                profit = max(values[ind]+prev[1], 0+prev[0]);
            
            cur[buy] = profit;
        }
        prev = cur;
    }
    return prev[1];
}

long getMaximumProfit(long *values, int n)
{
    //Write your code here
    //vector<vector<long>> dp(n, vector<long>(2, -1));
    return maxProfitSP(n, values);
}
