//Link = https://www.codingninjas.com/studio/problems/longest-increasing-subsequence_630459?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos&leftPanelTab=0

#include<bits/stdc++.h>

int lis(int ind, int prev_ind, int arr[], int n){
    if(ind==n) return 0;

    int notTake = 0 + lis(ind+1, prev_ind, arr, n);
    int take = 0;
    if(prev_ind==-1||arr[ind]>arr[prev_ind])
        take = 1 + lis(ind+1, ind, arr, n);
    
    return max(notTake, take);
}

int lisDP(int ind, int prev_ind, int arr[], int n, vector<vector<int>> &dp){
    if(ind==n+1) return 0;

    if(dp[ind][prev_ind]!=-1)
        return dp[ind][prev_ind];

    int notTake = 0 + lisDP(ind+1, prev_ind, arr, n, dp);
    int take= 0;
    if(prev_ind==0 || arr[ind-1]>arr[prev_ind-1])
        take = 1 + lisDP(ind+1, ind, arr, n, dp);

    return max(notTake, take);
}

int lisTB(int arr[], int n){
    vector<vector<int>> dp(n+2, vector<int>(n+1, 0));

    for(int ind=n;ind>=1;ind--){
        for(int prev_ind=0;prev_ind<ind;prev_ind++){
            int notTake = 0+dp[ind+1][prev_ind];
            int take= 0;
            if(prev_ind==0||arr[ind-1]>arr[prev_ind-1])
                take= 1 + dp[ind+1][ind];
            
            dp[ind][prev_ind] = max(notTake, take);
        }
    }
    return dp[1][0];
}

int lisSP(int arr[], int n){
    vector<int> after(n+1, 0), cur(n+1, 0);

    for(int ind=n;ind>=1;ind--){
        for(int prev_ind=0;prev_ind<ind;prev_ind++){
            int notTake = 0+after[prev_ind];
            int take= 0;
            if(prev_ind==0||arr[ind-1]>arr[prev_ind-1])
                take= 1 + after[ind];
            
            cur[prev_ind] = max(notTake, take);
        }
        after = cur;
    }
    return after[0];  
}

int longestIncreasingSubsequence(int arr[], int n)
{
    // Write Your Code here
    //vector<vector<int>> dp(n+1, vector<int>(n+1, -1));

    return lisSP(arr, n);
}

