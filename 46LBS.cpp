//Link = https://www.codingninjas.com/studio/problems/longest-bitonic-sequence_1062688?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos&leftPanelTabValue=PROBLEM

void lis(vector<int> &arr, int n, vector<int> &dp, int start){
	if(start==0){
		for(int ind=1;ind<n;ind++){
			for(int prev=0;prev<ind;prev++){
				if(arr[ind]>arr[prev] && dp[ind]<dp[prev]+1)
					dp[ind] = dp[prev]+1;
			}
		}
	}
	else{
		for(int ind=n-1;ind>=0;ind--){
			for(int after=n-1;after>ind;after--){
				if(arr[ind]>arr[after] && dp[ind]<dp[after]+1)
					dp[ind] = dp[after]+1;
			}
		}
	}
}

int longestBitonicSubsequence(vector<int>& arr, int n)
{
	// Write your code here.
	vector<int> dp1(n, 1);
	vector<int> dp2(n, 1);

	lis(arr, n, dp1, 0);
	lis(arr, n, dp2, n-1);

	int res = 0;

	for(int i=0;i<n;i++){
		res = max(res, dp1[i]+dp2[i]-1);
	}

	return res;
}

