// link = https://www.codingninjas.com/studio/problems/frog-jump_3621012?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos

#include <bits/stdc++.h> 
using namespace std;

int jump(int n, vector<int> &heights){
    if(n<=1)
        return abs(heights[n]-heights[0]);
    int cost1 = jump(n-1, heights) + abs(heights[n]-heights[n-1]);
    int cost2 = jump(n-2, heights) + abs(heights[n]-heights[n-2]);
    return min(cost1, cost2);
}

int jumpDp(int n, vector<int> &heights, vector<int> &dp){
    if(n<=1)
        return abs(heights[n]-heights[0]);
    if(dp[n]!=-1)
        return dp[n];
    int cost1 = jumpDp(n-1, heights, dp) + abs(heights[n]-heights[n-1]);
    int cost2 = jumpDp(n-2, heights, dp) + abs(heights[n]-heights[n-2]);
    return dp[n]=min(cost1, cost2);
}

int jumpTab(int n, vector<int> &heights){
    vector<int> dp(n+1);
    dp[0]=0;
    dp[1] = abs(heights[1]-heights[0]);
    for(int i=2;i<=n;i++){
        dp[i] = min(abs(heights[i]-heights[i-1])+dp[i-1], abs(heights[i]-heights[i-2])+dp[i-2]);
    }
    return dp[n];
}

int jumpSP(int n, vector<int> &heights){
    int prev2 = 0;
    int prev1 = abs(heights[1]-heights[0]);
    
    for(int i=2;i<=n;i++){
        int curr_loss = min(abs(heights[i]-heights[i-1])+prev1, abs(heights[i]-heights[i-2])+prev2);
        prev2 = prev1;
        prev1 = curr_loss;
    }
    return prev1;
}

int frogJump(int n, vector<int> &heights)
{
    // Write your code here.
    vector<int> dp(n, -1);
    //return jumpSP(n-1, heights);
    return jumpDp(n-1, heights, dp);
}

int main(){
    vector<int> heights = {10, 20, 30, 10};
    cout<<frogJump(4, heights);
}
