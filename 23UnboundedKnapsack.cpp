//Link = https://www.codingninjas.com/studio/problems/unbounded-knapsack_1215029?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos&leftPanelTab=0



int unbound(int ind, int target, vector<int> &weight, vector<int> &profit){
    if(ind==0){
        int num = target/weight[ind];
        return num*profit[ind];
    }

    int notTake = 0+unbound(ind-1, target, weight, profit);
    int take = 0;
    if(target>=weight[ind])
        take = profit[ind] + unbound(ind, target-weight[ind], weight, profit);
    
    return max(notTake, take);
}

int unboundDP(int ind, int target, vector<int> &weight, vector<int> &profit, vector<vector<int>> &dp){
    if(ind == 0){
        int num = target/weight[ind];
        return dp[ind][target] = num*profit[ind];
    }

    if(dp[ind][target]!=-1)
        return dp[ind][target];

    int notTake = 0+unboundDP(ind-1, target, weight, profit, dp);
    int take = 0;
    if(target>=weight[ind])
        take = profit[ind] + unboundDP(ind, target-weight[ind], weight, profit, dp);

    return dp[ind][target] = max(notTake, take);
}

int unboundTB(int n, int w, vector<int> &weight, vector<int> &profit){
    vector<vector<int>> dp(n, vector<int>(w+1, 0));

    for(int j=0;j<=w;j++){
        int num = j/weight[0];
        dp[0][j] = num*profit[0];
    }

    for(int ind=1;ind<n;ind++){
        for(int target=0;target<=w;target++){
            int notTake = 0+dp[ind-1][target];
            int take = 0;
            if(target>=weight[ind])
                take = profit[ind] + dp[ind][target-weight[ind]];
            
            dp[ind][target] = max(notTake, take);
        }
    }

    return dp[n-1][w];
}

int unboundSP(int n, int w, vector<int> &weight, vector<int> &profit){
    vector<int> prev(w+1, 0), cur(w+1, 0);

    for(int j=0;j<=w;j++){
        int num = j/weight[0];
        prev[j] = num*profit[0];
    }

    for(int ind=1;ind<n;ind++){
        for(int target=0;target<=w;target++){
            int notTake = 0+prev[target];
            int take = 0;
            if(target>=weight[ind])
                take = profit[ind] + cur[target-weight[ind]];
            
            cur[target] = max(notTake, take);
        }
        prev = cur;
    }

    return prev[w];
}

int unboundmSP(int n, int w, vector<int> &weight, vector<int> &profit){
    vector<int> dp(w+1, 0);

    for(int j=0;j<=w;j++){
        int num = j/weight[0];
        dp[j] = num*profit[0];
    }

    for(int ind=1;ind<n;ind++){
        for(int target=0;target<=w;target++){
            int notTake = 0+dp[target];
            int take = 0;
            if(target>=weight[ind])
                take = profit[ind] + dp[target-weight[ind]];
            
            dp[target] = max(notTake, take);
        }
    }

    return dp[w];
}

int unboundedKnapsack(int n, int w, vector<int> &profit, vector<int> &weight){
    // Write Your Code Here.
    //vector<vector<int>> dp(n, vector<int>(w+1, -1));
    return unboundmSP(n, w, weight, profit);
}
