//Link = https://www.codingninjas.com/studio/problems/wildcard-pattern-matching_701650?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos&leftPanelTab=0

#include<bits/stdc++.h>
bool wild(int i, int j, string &s, string &t){
   if(i<0 && j<0) return true;
   if(i<0 && j>=0) return false;
   if(i>=0 && j<0){
      for(int z = 0;z<=i;z++){
         if(s[z]!='*')
            return false;
      }
      return true;
   }

   if(s[i]==t[j] || s[i]=='?')
      return wild(i-1, j-1, s, t);
   if(s[i]=='*'){
      return wild(i-1, j, s, t) | wild(i, j-1, s, t);
   }
   return false;
}

int wildDP(int i, int j, string &s, string &t, vector<vector<int>> &dp){
   if(i<0 && j<0) return 1;
   if(i<0 && j>=0) return 0;
   if(i>=0 && j<0){
      for(int z=0;z<=i;z++){
         if(s[z]!='*')
            return 0;
      }
      return 1;
   }

   if(dp[i][j]==0)
      return 0;
   else if(dp[i][j]==1)
      return 1;

   if(s[i]==t[j] || s[i]=='?')
      return dp[i][j] = wildDP(i-1, j-1, s, t, dp);
   if(s[i] == '*')
      return dp[i][j] = wildDP(i-1, j, s, t, dp) | wildDP(i, j-1, s, t, dp);

   return 0;
}

bool wildTB(int n, int m, string &s, string &t){
   vector<vector<bool>> dp(n+1, vector<bool>(m+1, false));

   dp[0][0]=true;
   for(int j=1;j<=m;j++)
      dp[0][j] = false;

   for(int i=1;i<=n;i++){
      bool flag = true;
      for(int z=1;z<=i;z++){
         if(s[z-1]!='*'){
            flag = false;
            break;
         }
      }
      dp[i][0] = flag;
   }

   for(int i=1;i<=n;i++){
      for(int j=1;j<=m;j++){
         if(s[i-1]==t[j-1] || s[i-1]=='?')
            dp[i][j] = dp[i-1][j-1];
         else if(s[i-1]=='*')
            dp[i][j] = dp[i-1][j] | dp[i][j-1];
         else dp[i][j] = false;
      }
   }

   return dp[n][m];
}

bool wildSP(int n, int m, string &s, string &t){
   vector<bool> prev(m+1, false);

   prev[0] = true;
   for(int j=1;j<=m;j++)
      prev[j] = false;
   
   for(int i=1;i<=n;i++){
      vector<bool> cur(m+1, false);
      bool flag = true;
      for(int z=1;z<=i;z++){
         if(s[z-1]!='*'){
            flag = false;
            break;
         }
      }
      cur[0] = flag;
      for(int j=1;j<=m;j++){
         if(s[i-1]==t[j-1] || s[i-1]=='?')
            cur[j] = prev[j-1];
         else if(s[i-1]=='*')
            cur[j] = prev[j] | cur[j-1];
         else cur[j] = false;
      }
      prev = cur;
   }
   return prev[m];
}

bool wildcardMatching(string pattern, string text)
{
   // Write your code here.
   int n = pattern.size();
   int m = text.size();

   //vector<vector<int>> dp(n, vector<int>(m, -1));

   //if(wildDP(n-1, m-1, pattern, text, dp)==1)
   //   return true;
   //return false;
   return wildSP(n, m, pattern, text);
}

