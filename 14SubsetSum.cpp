//Link = https://www.codingninjas.com/studio/problems/subset-sum-equal-to-k_1550954?leftPanelTab=0&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos

#include <bits/stdc++.h> 

bool subset(int ind, int target, vector<int> &arr){
    if(target==0) return true;
    if(ind==0) return arr[ind]==target;

    bool notTake = subset(ind-1, target, arr);
    bool take = false;
    if(target>=arr[ind])
        take = subset(ind-1, target-arr[ind], arr);

    return notTake||take;
}

bool subsetDP(int ind, int target, vector<int> &arr, vector<vector<int>> &dp){
    if(target == 0){
        dp[ind][target]=1;
        return true;
    }
    if(ind==0){
        if(target == arr[ind]){
            dp[ind][target]=1;
            return true;
        }
        else{
            dp[ind][target]=0;
            return false;
        }
    }

    if(dp[ind][target]!=-1){
        if(dp[ind][target])
            return true;
        else return false;
    }
    
    bool notTake = subsetDP(ind-1, target, arr, dp);
    bool take = false;
    if(target>=arr[ind])
        take = subsetDP(ind-1, target-arr[ind], arr, dp);
    
    return notTake||take;
}

bool subsetTB(int n, int k, vector<int> &arr){
    vector<vector<bool>> dp(n, vector<bool>(k+1, false));
    for(int i=0;i<n;i++) dp[i][0] = true;
    if(arr[0]<=k) dp[0][arr[0]]=true;

    for(int ind=1;ind<n;ind++){
        for(int target=1;target<=k;target++){
            bool notTake = dp[ind-1][target];
            bool take = false;
            if(target>=arr[ind])
                take = dp[ind-1][target-arr[ind]];
            dp[ind][target] = notTake|take;
        }
    }

    return dp[n-1][k];
}

bool subsetSP(int n, int k, vector<int> &arr){
    vector<bool> prev(k+1, false);
    prev[0]=true;
    if(arr[0]<=k) prev[arr[0]] = true;

    for(int ind = 1;ind<n;ind++){
        vector<bool> cur(k+1, false);
        cur[0] = true;
        for(int target=1;target<=k;target++){
            bool notTake = prev[target];
            bool take = false;
            if(target>=arr[ind])
                take = prev[target-arr[ind]];
            cur[target] = notTake|take;
        }
        prev = cur;
    }

    return prev[k];
}

bool subsetSumToK(int n, int k, vector<int> &arr) {
    // Write your code here.
    //vector<vector<int>> dp(n+1, vector<int>(k+1, -1));
    return subsetSP(n, k, arr);
}
