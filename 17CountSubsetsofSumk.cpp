//Link = https://www.codingninjas.com/studio/problems/number-of-subsets_3952532?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos&leftPanelTab=0

int mod = 1e9+7;

int ways(int ind, int target, vector<int> &arr){
	if(ind==0){
		if(target==0 && arr[ind]==0)
			return 2;
		else if(target==0 || arr[ind]==target) return 1;
		else return 0;
	}

	int notTake = ways(ind-1, target, arr);
	int take = 0;
	if(target>=arr[ind])
		take = ways(ind-1, target-arr[ind], arr);
	return (notTake+take)%mod;
}

int waysDP(int ind, int target, vector<int> &arr, vector<vector<int>> &dp){
	if(ind==0){
		if(target == 0 && arr[ind]==0){
			return dp[ind][target]=2;
		}
		else if(target==0 || arr[ind]==target)
			return dp[ind][target]=1;
		else return dp[ind][target] = 0;
	}

	if(dp[ind][target]!=-1)
		return dp[ind][target];
	
	int notTake = waysDP(ind-1, target, arr, dp);
	int take = 0;
	if(target>=arr[ind])
		take = waysDP(ind-1, target-arr[ind], arr, dp);
	
	return dp[ind][target] = (notTake + take)%mod;
}

int waysTB(int n, int k, vector<int> &arr){
	vector<vector<int>> dp(n, vector<int>(k+1, 0));
	for(int j=0;j<=k;j++){
		if(j==0 && arr[0]==0)
			dp[0][j]=2;
		else if(j==0 || arr[0]==j)
			dp[0][j]=1;
		else dp[0][j]=0;
	}

	for(int ind=1;ind<n;ind++){
		for(int target=0;target<=k;target++){
			int notTake = dp[ind-1][target];
			int take = 0;
			if(target>=arr[ind])
				take = dp[ind-1][target-arr[ind]];
			dp[ind][target] = (notTake+take)%mod;
		}
	}
	return dp[n-1][k];
}

int waysSP(int n, int k, vector<int> &arr){
	vector<int> prev(k+1, 0);

	for(int j=0;j<=k;j++){
		if(j==0 && arr[0]==0)
			prev[j]=2;
		else if(j==0 || arr[0]==j)
			prev[j]=1;
		else prev[j]=0;
	}

	for(int ind=1;ind<n;ind++){
		vector<int> cur(k+1, 0);
		for(int target=0;target<=k;target++){
			int notTake = prev[target];
			int take = 0;
			if(target>=arr[ind])
				take = prev[target-arr[ind]];

			cur[target] = (notTake+take)%mod;
		}
		prev = cur;
	}

	return prev[k];
}

int findWays(vector<int>& arr, int k)
{
	// Write your code here.
	int n = arr.size();
	vector<vector<int>> dp(n, vector<int>(k+1, -1));
	return waysSP(n, k, arr);
}


