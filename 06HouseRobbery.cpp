#include <bits/stdc++.h> 

long long int robbing(vector<int> &house){
    int n = house.size();

    long long int prev1 = house[0];
    long long int prev2 = 0;

    for(int i=1;i<n;i++){
        int take = house[i] + prev2;
        int not_take = 0 + prev1;

        prev2 = prev1;
        prev1 = max(take, not_take);
    }
    return prev1;
}

long long int houseRobber(vector<int>& valueInHouse)
{
    // Write your code here.
    vector<int> houseList1, houseList2;

    int n = valueInHouse.size();
    for(int i=0;i<n;i++){
        if(i!=n-1)
            houseList1.push_back(valueInHouse[i]);
        if(i!=0)
            houseList2.push_back(valueInHouse[i]);
    }

    long long int take_first = robbing(houseList1);
    long long int take_last = robbing(houseList2);
    return max(take_first, take_last);
}
