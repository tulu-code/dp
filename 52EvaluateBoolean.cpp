//Link = https://www.codingninjas.com/studio/problems/problem-name-boolean-evaluation_1214650?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos&leftPanelTabValue=PROBLEM


int mod = 1000000007;

int evaluate(int i, int j, int isTrue, string &exp){
    if(i>j) return 0;

    if(i==j){
        if(isTrue) return exp[i]=='T';
        else return exp[i]=='F';
    }

    int ways =0;

    for(int k=i+1;k<=j-1;k+=2){
        int lT = evaluate(i, k-1, 1, exp)%mod;
        int lF = evaluate(i, k-1, 0, exp)%mod;
        int rT = evaluate(k+1, j, 1, exp)%mod;
        int rF = evaluate(k+1, j, 0, exp)%mod;

        if(exp[k]=='|'){
            if(isTrue) ways = (ways + (lT*rT)%mod + (lF*rT)%mod + (lT*rF)%mod)%mod;
            else ways = (ways + (lF*rF)%mod)%mod;
        }
        else if(exp[k]=='&'){
            if(isTrue) ways = (ways+ (lT*rT)%mod)%mod;
            else ways = (ways + (lT*rF)%mod + (lF*rT)%mod + (lF*rF)%mod)%mod;
        }
        else{
            if(isTrue) ways = (ways + (lT*rF)%mod + (lF*rT)%mod)%mod;
            else ways = (ways + (lF*rF)%mod + (lT*rT)%mod)%mod;
        }
    }
    return ways;
}

long long int evaluateDP(int i, int j, int isTrue, string &exp, vector<vector<vector<long long int>>> &dp){
    if(i>j) return dp[i][j][isTrue] = 0;

    if(i==j){
        if(isTrue) return dp[i][j][isTrue] = (exp[i]=='T');
        else return dp[i][i][isTrue] = (exp[i]=='F');
    }

    if(dp[i][j][isTrue]!=-1)
        return dp[i][j][isTrue];

    long long int ways =0;

    for(int k=i+1;k<=j-1;k+=2){
        long long int lT = evaluateDP(i, k-1, 1, exp, dp)%mod;
        long long int lF = evaluateDP(i, k-1, 0, exp, dp)%mod;
        long long int rT = evaluateDP(k+1, j, 1, exp, dp)%mod;
        long long int rF = evaluateDP(k+1, j, 0, exp, dp)%mod;

        if(exp[k]=='|'){
            if(isTrue) ways = (ways + (lT*rT)%mod + (lF*rT)%mod + (lT*rF)%mod)%mod;
            else ways = (ways + (lF*rF)%mod)%mod;
        }
        else if(exp[k]=='&'){
            if(isTrue) ways = (ways+ (lT*rT)%mod)%mod;
            else ways = (ways + (lT*rF)%mod + (lF*rT)%mod + (lF*rF)%mod)%mod;
        }
        else{
            if(isTrue) ways = (ways + (lT*rF)%mod + (lF*rT)%mod)%mod;
            else ways = (ways + (lF*rF)%mod + (lT*rT)%mod)%mod;
        }
    }
    return dp[i][j][isTrue] = ways;
}

int evaluateExp(string & exp) {
    // Write your code here.
    int n = exp.size();

    vector<vector<vector<long long int>>> dp(n, vector<vector<long long int>>(n, vector<long long int>(2, -1)));

    return evaluateDP(0, n-1, 1, exp, dp);
}
