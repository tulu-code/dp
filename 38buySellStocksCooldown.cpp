//Link = https://www.codingninjas.com/studio/problems/highway-billboards_3125969?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos&leftPanelTab=0

int stock(int ind, int buy, vector<int> &prices, int n){
    if(ind>=n) return 0;

    int profit = 0;
    if(buy)
        profit = max(-prices[ind] + stock(ind+1, 0, prices, n),
                    0 + stock(ind+1, 1, prices, n));
    else
        profit = max(prices[ind] + stock(ind+2, 1, prices, n),
                    0 + stock(ind+1, 0, prices, n));
    
    return profit;
}

int stockTB(vector<int> &prices){
    int n = prices.size();

    vector<vector<int>> dp(n+2, vector<int>(2, 0));

    for(int ind=n-1;ind>=0;ind--){
        for(int buy=0;buy<=1;buy++){
            int profit = 0;

            if(buy)
                profit = max(-prices[ind] + dp[ind+1][0],
                            0 + dp[ind+1][1]);
            else
                profit = max(prices[ind] + dp[ind+2][1],
                            0 + dp[ind+1][0]);
            
            dp[ind][buy] = profit;
        }
    }
    return dp[0][1];
}

int stockProfit(vector<int> &prices){
    // Write your code here.
    //int n=prices.size();
    //vector<vector<vector<int>>> dp(n, vector<vector<int>>(2, vector<int>(3, -1)));

    return stockTB(prices);
}
