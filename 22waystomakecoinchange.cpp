//Link = https://www.codingninjas.com/studio/problems/ways-to-make-coin-change_630471?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos

#include<bits/stdc++.h>
long count(int ind, int target, int *arr){
    if(ind==0){
        if(target%arr[ind]==0) return 1;
        else return 0;
    }

    long notTake = count(ind-1, target, arr);
    long take = 0;
    if(target>=arr[ind])
        take = count(ind, target-arr[ind], arr);
    
    return notTake+take;
}

long countDP(int ind, int target, int *arr, vector<vector<int>> &dp){
    if(ind==0){
        if(target%arr[ind]==0)
            return dp[ind][target]=1;
        else return dp[ind][target]=0;
    }

    if(dp[ind][target]!=-1)
        return dp[ind][target];
    
    long notTake = countDP(ind-1, target, arr, dp);
    long take = 0;
    if(target>=arr[ind])
        take = countDP(ind, target-arr[ind], arr, dp);

    return dp[ind][target] = notTake+take;
}

long countTB(int n, int k, int *arr){
    vector<vector<long>> dp(n, vector<long>(k+1, 0));

    for(int j=0;j<=k;j++)
        if(j%arr[0]==0)
            dp[0][j] = 1;
    
    for(int ind=1;ind<n;ind++){
        for(int target=0;target<=k;target++){
            long notTake = dp[ind-1][target];
            long take = 0;
            if(target>=arr[ind])
                take = dp[ind][target-arr[ind]];
            
            dp[ind][target] = notTake+take;
        }
    }

    return dp[n-1][k];
}

long countSP(int n, int k, int *arr){
    vector<long> prev(k+1, 0), cur(k+1, 0);

    for(int j=0;j<=k;j++)
        if(j%arr[0]==0)
            prev[j] = 1;
    
    for(int ind=1;ind<n;ind++){
        for(int target=0;target<=k;target++){
            long notTake = prev[target];
            long take = 0;
            if(target>=arr[ind])
                take = cur[target-arr[ind]];

            cur[target] = notTake+take;
        }
        prev = cur;
    }

    return prev[k];
}

long countWaysToMakeChange(int *denominations, int n, int value)
{
    //Write your code here
    //vector<vector<long>> dp(n, vector<long>(value+1, -1));
    return countSP(n, value, denominations);
}
