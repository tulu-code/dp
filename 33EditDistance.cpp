//Link = https://www.codingninjas.com/studio/problems/edit-distance_630420?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos&leftPanelTab=0


int dis(int i, int j, string &s, string &t){
    if(i<0) return j+1;
    if(j<0) return i+1;

    if(s[i]==t[j])
        return 0 + dis(i-1, j-1, s, t);
    else{
        int insertion = 1 + dis(i, j-1, s, t);
        int deletion = 1 + dis(i-1, j, s, t);
        int replace = 1 + dis(i-1, j-1, s, t);

        return min(min(insertion, deletion), replace);
    }
}

int disDP(int i, int j, string &s, string &t, vector<vector<int>> &dp){
    if(i<0) return j+1;
    if(j<0) return i+1;

    if(dp[i][j]!=-1)
        return dp[i][j];

    if(s[i]==t[j])
        return dp[i][j] = 0 + dis(i-1, j-1, s, t);
    else{
        int insertion = 1 + dis(i, j-1, s, t);
        int deletion = 1 + dis(i-1, j, s, t);
        int replace = 1 + dis(i-1, j-1, s, t);

        return dp[i][j] = min(min(insertion, deletion), replace);
    }
}

int disTB(int n, int m, string &s, string &t){
    vector<vector<int>> dp(n+1, vector<int>(m+1, 0));

    for(int j=0;j<=m;j++)
        dp[0][j] = j;
    for(int i=1;i<=n;i++)
        dp[i][0] = i;

    for(int i=1;i<=n;i++){
        for(int j=1;j<=m;j++){
            if(s[i-1]==t[j-1])
                dp[i][j] = dp[i-1][j-1];
            else{
                int insertion = 1 + dp[i][j-1];
                int deletion = 1 + dp[i-1][j];
                int replace = 1 + dp[i-1][j-1];

                dp[i][j] = min(min(insertion, deletion), replace);
            }
        }
    }    
    return dp[n][m];
}

int disSP(int n, int m, string &s, string &t){
    vector<int> prev(m+1, 0);

    for(int j=0;j<=m;j++)
        prev[j] = j;

    for(int i=1;i<=n;i++){
        vector<int> cur(m+1, 0);
        cur[0] = i;
        for(int j=1;j<=m;j++){
            if(s[i-1]==t[j-1])
                cur[j] = prev[j-1];
            else{
                int insertion = 1 + cur[j-1];
                int deletion = 1 + prev[j];
                int replace = 1 + prev[j-1];

                cur[j] = min(min(insertion, deletion), replace);
            }
        }
        prev = cur;
    }    
    return prev[m];
}

int editDistance(string str1, string str2)
{
    //write you code here
    int n = str1.size();
    int m = str2.size();

    //vector<vector<int>> dp(n, vector<int>(m, -1));

    return disSP(n, m, str1, str2);
}
