//Link = https://www.codingninjas.com/studio/problems/minimum-elements_3843091?leftPanelTab=0&campaign=striver_dpseries&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dpseries

#include <bits/stdc++.h> 

int minimum(int ind, int target, vector<int> &num){
    if(ind==0){
        if(target%num[ind]==0)
            return target/num[ind];
        else return 1e9;
    }

    int notTake = 0+minimum(ind-1, target, num);
    int take = INT_MAX;
    if(target>=num[ind])
        take = 1+minimum(ind, target-num[ind], num);
    
    return min(notTake, take);
}

int minimumDP(int ind, int target, vector<int> &num, vector<vector<int>> &dp){
    if(ind==0){
        if(target%num[ind]==0)
            return dp[ind][target] = target/num[ind];
        else return dp[ind][target] =1e9;
    }
    if(dp[ind][target]!=-1)
        return dp[ind][target];
    
    int notTake = 0+minimumDP(ind-1, target, num, dp);
    int take = INT_MAX;
    if(target>=num[ind])
        take = 1+minimumDP(ind, target-num[ind], num, dp);

    return min(notTake, take);
}

int minimumTB(int n, int x, vector<int> &num){
    vector<vector<int>> dp(n, vector<int>(x+1, 0));

    for(int j=0;j<=x;j++){
        if(j%num[0]==0)
            dp[0][j] = j/num[0];
        else dp[0][j] = 1e9;
    }

    for(int ind=1;ind<n;ind++){
        for(int target=0;target<=x;target++){
            int notTake = 0 + dp[ind-1][target];
            int take = INT_MAX;
            if(target>=num[ind])
                take = 1+ dp[ind][target-num[ind]];

            dp[ind][target] = min(notTake, take);
        }
    }

    return dp[n-1][x];
}

int minimumSP(int n, int x, vector<int> &num){
    vector<int> prev(x+1, 0), cur(x+1, 0);

    for(int j=0;j<=x;j++){
        if(j%num[0]==0){
            prev[j] = j/num[0];
        }
        else prev[j] = 1e9;
    }

    for(int ind=1;ind<n;ind++){
        for(int target=0;target<=x;target++){
            int notTake = 0 + prev[target];
            int take = INT_MAX;
            if(target>=num[ind])
                take = 1+cur[target-num[ind]];

            cur[target] = min(notTake, take);
        }
        prev = cur;
    }

    return prev[x];
}

int minimumElements(vector<int> &num, int x)
{
    // Write your code here.
    int n = num.size();

    //vector<vector<int>> dp(n, vector<int>(x+1, -1));
    int ans = minimumSP(n, x, num);
    return ans>=1e9?-1:ans;
}
