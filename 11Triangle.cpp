//link = https://www.codingninjas.com/studio/problems/triangle_1229398?interviewProblemRedirection=true
#include <bits/stdc++.h> 

int minPathSum(int row, int col, int n, vector<vector<int>> &triangle){
	if(row==(n-1))
		return triangle[row][col];
	int d = triangle[row][col] + minPathSum(row+1, col, n, triangle);
	int dg = triangle[row][col] + minPathSum(row+1, col+1, n, triangle);
	return min(d, dg);
}

int minPathSumDP(int row, int col, int n, vector<vector<int>> &triangle, vector<vector<int>> &dp){
	if(row==(n-1))
		return dp[row][col] = triangle[row][col];
	if(dp[row][col]!=-1)
		return dp[row][col];

	int d = triangle[row][col] + minPathSum(row+1, col, n, triangle);
	int dg = triangle[row][col] + minPathSum(row+1, col+1, n, triangle);

	return dp[row][col] = min(d, dg);
}

int minPathSumTB(int n, vector<vector<int>> &triangle){
	vector<vector<int>> dp(n, vector<int>(n, -1)); //rowno = no of columns

	for(int j=0;j<n;j++){
		dp[n-1][j] = triangle[n-1][j];
	}
	
	for(int i=n-2;i>=0;i--){
		for(int j=i;j>=0;j--){
			int d = triangle[i][j] + dp[i+1][j];
			int dg = triangle[i][j] + dp[i+1][j+1];
			dp[i][j] = min(d, dg);
		}
	}
	return dp[0][0];
}

int minPathSumSP(int n, vector<vector<int>> &triangle){
	vector<int> prev(n, -1);

	for(int j=0;j<n;j++){
		prev[j] = triangle[n-1][j];
	}

	for(int i=n-2;i>=0;i--){
		vector<int> cur(n, -1);
		for(int j=i;j>=0;j--){
			int d = triangle[i][j] + prev[j];
			int dg = triangle[i][j] + prev[j+1];
			cur[j] = min(d, dg);
		}
		prev = cur;
	}
	return prev[0];
}

int minimumPathSum(vector<vector<int>>& triangle, int n){
	// Write your code here.
	//vector<vector<int>> dp(n, vector<int>(n, -1));
	return minPathSumSP(n, triangle);
}
