//Link = https://www.codingninjas.com/studio/problems/partition-a-set-into-two-subsets-such-that-the-difference-of-subset-sums-is-minimum_842494?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos&leftPanelTab=0

vector<bool> subset(int n, int k, vector<int> &arr){
	vector<bool> prev(k+1, false);
	prev[0] = true;
	if(arr[0]<=k) prev[arr[0]] = true;

	for(int ind=1;ind<n;ind++){
		vector<bool> cur(k+1, false);
		cur[0]=true;
		for(int target=1;target<=k;target++){
			bool notTake = prev[target];
			bool take = false;
			if(target>=arr[ind])
				take = prev[target-arr[ind]];
			cur[target] = notTake|take;
		}
		prev=cur;
	}

	return prev;
}

int minSubsetSumDifference(vector<int>& arr, int n)
{
	// Write your code here.
	int totalSum = 0;
	for(int i=0;i<n;i++)
		totalSum+= arr[i];
	int target = totalSum/2;
	vector<bool> lastrow = subset(n, target, arr);
	
	int diff = INT_MAX;
	for(int i=0;i<=target;i++){
		if(lastrow[i]){
			int s1Sum = i;
			int s2Sum = totalSum-i;
			diff = min(diff, abs(s1Sum-s2Sum));
		}
	}
	return diff;
}

