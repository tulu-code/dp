//Link = https://www.codingninjas.com/studio/problems/matrix-chain-multiplication_975344?source=youtube&campaign=striver_dp_videos&utm_source=youtube&utm_medium=affiliate&utm_campaign=striver_dp_videos&leftPanelTabValue=PROBLEM

#include <bits/stdc++.h> 

int mcmDP(int i, int j, vector<int> &arr, vector<vector<int>> &dp){
    if(i==j) return dp[i][j] = 0;

    if(dp[i][j]!=-1)
        return dp[i][j];

    int mini = INT_MAX;
    for(int k=i;k<j;k++){
        int steps = arr[i-1]*arr[k]*arr[j] + mcmDP(i, k, arr, dp) + mcmDP(k+1, j, arr, dp);
        mini = min(mini, steps);
    }
    return dp[i][j] = mini;
}

int mcmTB(vector<int> &arr, int n){
    vector<vector<int>> dp(n, vector<int>(n, 0));

    for(int i=0;i<n;i++)
        dp[i][i]=0;
    
    for(int i=n-1;i>=1;i--){
        for(int j=i+1;j<n;j++){
            int mini = INT_MAX;
            for(int k=i;k<j;k++){
                int steps = arr[i-1]*arr[k]*arr[j] + dp[i][k] + dp[k+1][j];
                mini = min(mini, steps);
            }
            dp[i][j] = mini;
        }
    }
    return dp[1][n-1];
}

int matrixMultiplication(vector<int> &arr, int N)
{
    // Write your code here.
    vector<vector<int>> dp(N, vector<int>(N, -1));
    return mcmTB(arr, N);
}
